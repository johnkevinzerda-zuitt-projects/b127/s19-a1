const movies = [
{
	title:'Excorcist',
	genre: 'horror',
	dateReleased: '1972',
	rating: 5,
	displayRating:function(){
		console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
	},

},
{
	title:'Excorcist 2',
	genre: 'horror',
	dateReleased: '1975',
	rating: 2,
	displayRating:function(){
		console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
	}
},
{
	title:'Star Wars',
	genre: 'Sci-fi',
	dateReleased: '1977',
	rating: 4.5,
	displayRating:function(){
		console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
	}
},
{
	title:'Toy Story',
	genre: 'family/commedy',
	dateReleased: '1995',
	rating: 4.6,
	displayRating:function(){
		console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
	}
},
{
	title:'Deadpool',
	genre: 'action',
	dateReleased: '2016',
	rating: 4.3,
	displayRating:function(){
		console.log('The movie ' + this.title + ' has' + this.rating + 'stars')
	}
}

]

const showAllMovies = () => {for(let i=0; i < movies.length; i++) {
	console.log(`${movies[i].title} ${movies[i].genre}`)
	}
}

movies.sort((a,b) => a.rating-b.rating);

const showTitles = (rate)=>{
	if (rate<movies[0].rating || rate>movies[(movies.length-1)].rating) {
		console.log(`No movies with that rating.`)
	}else {
		console.log(`Movies with ${rate} and above ratings:`);
		let list=1;
		for(i=0;i<movies.length;i++){
			if (movies[i].rating>=rate) {
				console.log(`${list}) ${movies[i].title} ${movies[i].rating} stars`);
				list++;
			}
		}
	}
}